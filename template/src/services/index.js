// import { HTTP } from '@/api/http-common'
export default {
  getLeaveDashboardData () {
    let data = {
      ratio: {
        labels: ['Work', 'Holiday', 'Rest', 'Leave'],
        data: [250 + this.getRandomInt(), this.getRandomInt(), this.getRandomInt(), this.getRandomInt()]
      },
      distribute: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        data: [0, this.getRandomInt(), this.getRandomInt(), this.getRandomInt(), this.getRandomInt(), 0, this.getRandomInt(), 0, this.getRandomInt(), 0, this.getRandomInt(), 0]
      },
      annualLeave: {
        labels: ['Approved', 'Pending', 'Balance', 'Carry'],
        data: [this.getRandomInt(), this.getRandomInt(), this.getRandomInt(), this.getRandomInt()]
      }
    }
    return new Promise((resolve) => {
      setTimeout(() => resolve(data), 200)
    })
  },
  getRandomInt () {
    return Math.floor(Math.random() * (5 + 1)) + 5
  }
}
