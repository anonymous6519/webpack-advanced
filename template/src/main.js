{{#if_eq build "standalone"}}
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
{{/if_eq}}
import Vue from 'vue'{{#if_eq lintConfig "airbnb"}};{{/if_eq}}
import Vuex from 'vuex'{{#if_eq lintConfig "airbnb"}};{{/if_eq}}
import Vuetify from 'vuetify'
import App from './App'{{#if_eq lintConfig "airbnb"}};{{/if_eq}}
{{#router}}
import router from './router'{{#if_eq lintConfig "airbnb"}};{{/if_eq}}
{{/router}}

import vuexI18n from 'vuex-i18n'

import translationsEn from '@/assets/i18n/en'
import translationsTh from '@/assets/i18n/th'

import ErpVuePlugin from 'erp-vue-plugin'
import 'erp-vue-plugin/dist/erp-vue-plugin.css'

import pdfMake from 'pdfmake/build/pdfmake.js'
import vfsFonts from '@/assets/vfs_fonts'

const fonts = {
  THSarabun: {
    normal: 'THSarabun.ttf',
    bold: 'THSarabun Bold.ttf',
    italics: 'THSarabun Italic.ttf',
    bolditalics: 'THSarabun BoldItalic.ttf'
  },
  THSarabun_2: {
    normal: 'THSarabun_2.ttf',
    bold: 'THSarabun Bold_2.ttf',
    italics: 'THSarabun Italic_2.ttf',
    bolditalics: 'THSarabun BoldItalic_2.ttf'
  }
}
pdfMake.vfs = vfsFonts.pdfMake.vfs
pdfMake.fonts = fonts

Vue.use(ErpVuePlugin)

Vue.use(Vuex)
const store = new Vuex.Store()

Vue.use(vuexI18n.plugin, store)
Vue.i18n.add('en', translationsEn)
Vue.i18n.add('th', translationsTh)
Vue.i18n.set('en')

Vue.use(Vuetify)
Vue.config.productionTip = false{{#if_eq lintConfig "airbnb"}};{{/if_eq}}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  {{#router}}
  router,
  {{/router}}
  {{#if_eq build "runtime"}}
  render: h => h(App){{#if_eq lintConfig "airbnb"}},{{/if_eq}}
  {{/if_eq}}
  {{#if_eq build "standalone"}}
  template: '<App/>',
  components: { App }{{#if_eq lintConfig "airbnb"}},{{/if_eq}}
  {{/if_eq}}
}){{#if_eq lintConfig "airbnb"}};{{/if_eq}}
